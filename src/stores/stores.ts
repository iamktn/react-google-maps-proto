import { action, IObservableArray, observable } from 'mobx'
import { LandfillModel, Position, ProjectModel } from '../models/poiModels'

export class Stores {
    _directionService: google.maps.DirectionsService | null = null
    lastProjectSelected: ProjectModel | null = null

    constructor() {
        const projects: ProjectModel[] = [
            new ProjectModel('Izhevsk project', {
                lat: 56.84976,
                lng: 53.20448,
            }),
            new ProjectModel('Votkinsk project', {
                lat: 57.05172,
                lng: 53.987389,
            }),
            new ProjectModel('Igra project', {
                lat: 57.5460688,
                lng: 53.0276223,
            }),
        ]

        const landfills: LandfillModel[] = [
            new LandfillModel('Juski', { lat: 57.93251, lng: 53.77123 }),
            new LandfillModel('Balezino', { lat: 57.9813227, lng: 52.9723401 }),
            new LandfillModel('Sharkan', { lat: 57.302568, lng: 53.8284657 }),
            new LandfillModel('Vavozh', { lat: 56.7770787, lng: 51.9108313 }),
            new LandfillModel('Arzamastsevo', {
                lat: 56.1492397,
                lng: 53.683188,
            }),
        ]

        this.setProjects(projects)
        this.setLandfills(landfills)

        this.lastProjectSelected = projects[0]
    }

    get directionService(): google.maps.DirectionsService {
        if (!this._directionService) {
            this._directionService = new google.maps.DirectionsService()
        }
        return this._directionService
    }

    _createRoute(
        start: Position,
        end: Position,
        travelMode: google.maps.TravelMode,
        callback: (arg0: google.maps.DirectionsResult | null) => void
    ) {
        this.directionService.route(
            { origin: start, destination: end, travelMode },
            (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                    callback(result)
                } else {
                    callback(null)
                }
            }
        )
    }

    @action
    setDirection(newValue: google.maps.DirectionsResult | null) {
        this.direction = newValue
    }

    clearSelection() {
        this.selectedLandfill = null
        this.selectedProject = null
        this.direction = null
    }

    _updateDirections() {
        if (this.selectedLandfill && this.selectedProject) {
            this._createRoute(
                this.selectedProject.position,
                this.selectedLandfill.position,
                google.maps.TravelMode.DRIVING,
                this.setDirection.bind(this)
            )
            return
        }

        this.setDirection(null)
    }

    @observable
    selectedProject: ProjectModel | null = null

    @observable
    selectedLandfill: LandfillModel | null = null

    @observable
    direction: google.maps.DirectionsResult | null = null

    @observable
    newObjectPosition: Position | null = null

    @action
    setNewObjectPosition(lat: number | null = null, lng: number | null = null) {
        if (lat && lng) {
            this.newObjectPosition = {
                lng,
                lat,
            }

            this.tempNewObjectLat = lat
            this.tempNewObjectLng = lng
        } else {
            this.newObjectPosition = null
        }

        this.clearSelection()
    }

    @action resetNewObject() {
        this.newObjectPosition = null
        this.tempNewObjectLat = null
        this.tempNewObjectLng = null
    }

    @action
    selectLandfill(landfill: LandfillModel) {
        this.selectedLandfill = landfill
        this.resetNewObject()
        this._updateDirections()
    }

    @action selectProject(project: ProjectModel) {
        this.selectedProject = project
        this.resetNewObject()
        this._updateDirections()
    }

    @action
    setLandfills(landfills: LandfillModel[]) {
        this.landfills.replace(landfills)
    }

    @action
    setProjects(projects: ProjectModel[]) {
        this.projects.replace(projects)
    }

    updateNewObjectLat(value: number) {
        this.tempNewObjectLat = value
    }

    updateNewObjectLng(value: number) {
        this.tempNewObjectLng = value
    }

    @observable
    tempNewObjectLat: number | null = null

    @observable
    tempNewObjectLng: number | null = null

    landfills: IObservableArray<LandfillModel> = observable([])
    projects: IObservableArray<ProjectModel> = observable([])
}

export const poiStore = new Stores()
