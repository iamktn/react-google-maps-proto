import React from 'react'
import { observer } from 'mobx-react'
import { poiStore } from '../stores/stores'
import { MarkerType } from '../common/enums'
import { GoogleMap, InfoBox, LoadScript, Marker } from '@react-google-maps/api'
import RouteRenderer from './RouteRenderer'
import ProjectMarkerInfo from './ProjectMarkerInfo'
import {
    getDistanceForDirection,
    getIconByType,
    getMiddleRoutePosition,
} from '../common/utils'
import LandfillMarkerInfo from './LandfillMarkerInfo'
import { toJS } from 'mobx'
import { v4 as uuidv4 } from 'uuid'

const containerStyle = {
    width: '600',
    height: '400px',
}

const Markers = observer(() => {
    return (
        <>
            {poiStore.projects.map((p) => {
                return (
                    <ProjectMarkerInfo
                        project={p}
                        isSelected={p.id === poiStore.selectedProject?.id}
                    />
                )
            })}
            {poiStore.landfills.map((l) => {
                return (
                    <LandfillMarkerInfo
                        landfill={l}
                        isSelected={l.id === poiStore.selectedLandfill?.id}
                    />
                )
            })}

            {poiStore.newObjectPosition ? (
                <Marker
                    position={poiStore.newObjectPosition}
                    icon={getIconByType(MarkerType.NEW_OBJECT)}
                />
            ) : (
                <></>
            )}
        </>
    )
})

const RouteLengthInfo = observer(() => {
    if (!poiStore.direction) {
        return null
    }

    const direction = toJS(poiStore.direction!)
    return (
        <Marker
            position={getMiddleRoutePosition(direction)}
            visible={false}
            key={uuidv4()}
        >
            <InfoBox>
                <h4>{getDistanceForDirection(direction)}</h4>
            </InfoBox>
        </Marker>
    )
})

const options: google.maps.MapOptions = {
    streetViewControl: false,
    fullscreenControl: false,
    mapTypeControl: false,
}

@observer
class MapContent extends React.Component {
    onMapClick = (e: google.maps.MouseEvent) => {
        poiStore.setNewObjectPosition(e.latLng.lat(), e.latLng.lng())
    }

    render() {
        const center = poiStore.lastProjectSelected!.position
        return (
            <LoadScript googleMapsApiKey="AIzaSyA_whVcqTSuCujy-8Ls4QAj_rY4vCAnYsE">
                <GoogleMap
                    mapContainerStyle={containerStyle}
                    zoom={7}
                    center={center}
                    options={options}
                    onClick={this.onMapClick}
                >
                    <Markers />
                    <RouteRenderer />
                    <RouteLengthInfo />
                </GoogleMap>
            </LoadScript>
        )
    }
}

export default MapContent
