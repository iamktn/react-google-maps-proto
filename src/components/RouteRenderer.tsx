import React from 'react'
import { observer } from 'mobx-react'
import { DirectionsRenderer } from '@react-google-maps/api'
import { toJS } from 'mobx'
import { poiStore } from '../stores/stores'

@observer
class RouteRenderer extends React.Component {
    render() {
        if (!poiStore.direction) return null

        const directions = toJS(poiStore.direction)
        return (
            <DirectionsRenderer
                directions={directions}
                options={{ suppressMarkers: true, preserveViewport: true }}
            />
        )
    }
}

export default RouteRenderer
