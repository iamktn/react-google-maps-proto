import React from 'react'
import { LandfillModel } from '../models/poiModels'
import { observer } from 'mobx-react'

export type LandfillDataProps = {
    data: LandfillModel | null
}

@observer
export class LandfillInfo extends React.Component<LandfillDataProps> {
    render() {
        if (!this.props.data) return <h3>No landfill selected</h3>

        return <h3>{this.props.data.name}</h3>
    }
}
