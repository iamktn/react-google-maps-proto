import { LandfillInfo } from './LandfillInfo'
import { poiStore } from '../stores/stores'
import React from 'react'
import { observer } from 'mobx-react'
import { ProjectInfo } from './ProjectInfo'
import RouteInfo from './RouteInfo'
import ManualPosition from './ManualPosition'
import MapContent from './MapContent'

@observer
class MainArea extends React.Component {
    render() {
        return (
            <div>
                <LandfillInfo data={poiStore.selectedLandfill} />
                <ProjectInfo data={poiStore.selectedProject} />
                <RouteInfo />
                <MapContent />
                <ManualPosition />
            </div>
        )
    }
}

export default MainArea
