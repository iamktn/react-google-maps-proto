import React from 'react'
import { LandfillModel } from '../models/poiModels'
import { InfoWindow, Marker } from '@react-google-maps/api'
import { poiStore } from '../stores/stores'
import { MarkerType } from '../common/enums'
import { getIconByType } from '../common/utils'

type LandfillMarkerInfoProps = {
    landfill: LandfillModel
    isSelected: boolean
}

export default class LandfillMarkerInfo extends React.Component<
    LandfillMarkerInfoProps
> {
    render() {
        const { landfill, isSelected } = this.props

        return (
            <Marker
                position={landfill.position}
                key={landfill.id}
                icon={getIconByType(
                    isSelected
                        ? MarkerType.SELECTED_LANDFILL
                        : MarkerType.LANDFILL
                )}
                onClick={() => {
                    poiStore.selectLandfill(landfill)
                }}
            >
                {isSelected && (
                    <InfoWindow>
                        <h4>{landfill.name}</h4>
                    </InfoWindow>
                )}
            </Marker>
        )
    }
}
