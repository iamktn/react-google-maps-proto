import { observer } from 'mobx-react'
import React from 'react'
import { poiStore } from '../stores/stores'

@observer
export default class ManualPosition extends React.Component {
    onApplyCoords() {
        poiStore.setNewObjectPosition(
            poiStore.tempNewObjectLat,
            poiStore.tempNewObjectLng
        )
    }

    onChangeLat(e: React.ChangeEvent<HTMLInputElement>) {
        const value = Number(e.target.value)
        if (value) {
            poiStore.updateNewObjectLat(value)
        }
    }

    onChangeLng(e: React.ChangeEvent<HTMLInputElement>) {
        const value = Number(e.target.value)
        if (value) {
            poiStore.updateNewObjectLng(value)
        }
    }

    render() {
        let lat: number | string = ''
        let lng: number | string = ''

        if (poiStore.newObjectPosition) {
            lat = poiStore.tempNewObjectLat!
            lng = poiStore.tempNewObjectLng!
        }

        return (
            <div>
                <h3>Position</h3>
                <input type="text" value={lat} onChange={this.onChangeLat} />
                <input type="text" value={lng} onChange={this.onChangeLng} />
                <button onClick={this.onApplyCoords}>Apply</button>
            </div>
        )
    }
}
