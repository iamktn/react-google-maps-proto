import React from 'react'
import { ProjectModel } from '../models/poiModels'
import { InfoWindow, Marker } from '@react-google-maps/api'
import { poiStore } from '../stores/stores'
import { MarkerType } from '../common/enums'
import { getIconByType } from '../common/utils'

type ProjectMarkerInfoProps = {
    project: ProjectModel
    isSelected: boolean
}

export default class ProjectMarkerInfo extends React.Component<
    ProjectMarkerInfoProps
> {
    render() {
        const { project, isSelected } = this.props

        return (
            <Marker
                position={project.position}
                key={project.id}
                icon={getIconByType(
                    isSelected
                        ? MarkerType.SELECTED_PROJECT
                        : MarkerType.PROJECT
                )}
                onClick={() => {
                    poiStore.selectProject(project)
                }}
            >
                {isSelected && (
                    <InfoWindow>
                        <h3>{project.name}</h3>
                    </InfoWindow>
                )}
            </Marker>
        )
    }
}
