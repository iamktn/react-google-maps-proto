import React from 'react'
import { observer } from 'mobx-react'
import { poiStore } from '../stores/stores'
import { getDistanceForDirection } from '../common/utils'

@observer
export default class RouteInfo extends React.Component {
    render() {
        if (poiStore.direction) {
            return <h3>{getDistanceForDirection(poiStore.direction)} km</h3>
        } else {
            return <h3>Route not selected</h3>
        }
    }
}
