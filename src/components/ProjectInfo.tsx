import React from 'react'
import { ProjectModel } from '../models/poiModels'
import { observer } from 'mobx-react'

export type ProjectDataProps = {
    data: ProjectModel | null
}

@observer
export class ProjectInfo extends React.Component<ProjectDataProps> {
    render() {
        if (!this.props.data) return <h3>No project selected</h3>

        return <h3>{this.props.data.name}</h3>
    }
}
