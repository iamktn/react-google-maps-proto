import { MarkerType } from './enums'
import { Position } from '../models/poiModels'

const iconsByType = new Map<MarkerType, string>([
    [
        MarkerType.LANDFILL,
        'http://maps.google.com/mapfiles/ms/micons/orange.png',
    ],
    [
        MarkerType.SELECTED_LANDFILL,
        'http://maps.google.com/mapfiles/ms/micons/blue.png',
    ],
    [
        MarkerType.PROJECT,
        'http://maps.google.com/mapfiles/ms/micons/grn-pushpin.png',
    ],
    [
        MarkerType.SELECTED_PROJECT,
        'http://maps.google.com/mapfiles/ms/micons/red-pushpin.png',
    ],
    [
        MarkerType.NEW_OBJECT,
        'http://maps.google.com/mapfiles/kml/paddle/wht-stars.png',
    ],
])

export function getIconByType(type: MarkerType): string {
    const result = iconsByType.get(type)
    if (!result) throw new Error('Icon not found')
    return result
}

export function getDistanceForDirection(
    direction: google.maps.DirectionsResult
): number {
    return (
        direction.routes[0].legs.reduce((a, l) => a + l.distance.value, 0) /
        1000
    )
}

export function getMiddleRoutePosition(
    direction: google.maps.DirectionsResult
): Position {
    const point = direction.routes[0].bounds.getCenter()

    return {
        lat: point.lat(),
        lng: point.lng(),
    }
}
