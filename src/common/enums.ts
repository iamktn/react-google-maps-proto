export enum MarkerType {
    LANDFILL,
    PROJECT,
    SELECTED_LANDFILL,
    SELECTED_PROJECT,
    NEW_OBJECT,
}
