import { v4 as uuidv4 } from 'uuid'

export type Position = {
    lat: number
    lng: number
}

export class ProjectModel {
    id: string
    name: string
    position: Position
    startDate: Date | null
    endDate: Date | null

    constructor(
        name: string,
        position: Position,
        startDate: Date | null = null,
        endDate: Date | null = null
    ) {
        this.id = uuidv4()
        this.name = name
        this.position = position
        this.startDate = startDate
        this.endDate = endDate
    }
}

export class LandfillModel {
    id: string
    name: string
    position: Position

    constructor(name: string, position: Position) {
        this.id = uuidv4()
        this.name = name
        this.position = position
    }
}
